require_dependency 'machine_learning/analogies'
require_dependency 'machine_learning/base'
require_dependency 'machine_learning/image_searcher'
require_dependency 'machine_learning/image_search_base'
require_dependency 'machine_learning/embeddings_based_search'
require_dependency 'machine_learning/image'
require_dependency 'machine_learning/description'
require_dependency 'machine_learning/no_dataset_error'

module MachineLearning

end
