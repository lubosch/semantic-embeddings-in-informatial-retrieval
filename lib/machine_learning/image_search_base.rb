module MachineLearning
  class ImageSearchBase
    include Singleton

    attr_accessor :u, :v, :images, :test, :train

    def initialize
      @images = {}
      @train = []
      @test = {}

      preload_u
      preload_v
      load_test_train_data
      preload_image_vectors

    end

    def doc_id_to_image_id(doc_id)
      doc_id.strip.gsub(/#\d+/, '')
    end

    def train_images
      @train_images ||= @images.select { |key, _value| @train.include?(key) }
    end

    def test_images
      @test_images ||= @images.select { |key, _value| @test.keys.include?(key) }
    end

    def test_descriptions
      @descriptions = test_images.map { |_id, image| image.descriptions }.flatten
    end

    def preload_u
      u = []
      File.open(File.expand_path('public/datasets/U.csv', Rails.root)).each_line { |line| u << line.split.map(&:to_f) }
      @u = NMatrix[*u].transpose
    end

    def preload_v
      v = []
      File.open(File.expand_path('public/datasets/V.csv', Rails.root)).each_line { |line| v << line.split.map(&:to_f) }
      @v = NMatrix[*v].transpose
    end

    def preload_image_vectors
      vectors = File.open(File.expand_path('public/datasets/vgg_feats_transpose.csv', Rails.root))
      File.open(File.expand_path('public/datasets/image_files.txt', Rails.root)).each_line do |line|
        vector = NVector.to_na(vectors.readline.split.map(&:to_f))
        @images[line.strip] = Image.new(line.strip, vector, @v)
      end
    end

    def pre_train!
      train_data = File.open(File.expand_path("public/datasets/Flickr8k.lemma.token.txt", Rails.root))
      train_data.each do |line|
        document = line.split("\t")

        image_id = doc_id_to_image_id(document[0])
        next if @train.exclude?(image_id) && @test.keys.exclude?(image_id)
        image = @images[image_id]
        image.add_description(document[1], nil, @u)
      end

    end

    def search(*params)
    end

    def benchmark(*params)
      raise NoDatasetLoaded if Analogies.instance.matrix.blank?
      results = {count: 0, top1: 0, top5: 0, top10: 0, top15: 0, top20: 0, mrr: 0}
      @test.each do |id, query|
        rank_list = search(query, *params)
        results[:count] += 1

        rank = rank_list.find_index { |r| r[0] == id } + 1

        results[:top1] += 1 if rank == 1
        results[:top5] += 1 if rank <= 5
        results[:top10] += 1 if rank <= 10
        results[:top15] += 1 if rank <= 15
        results[:top20] += 1 if rank <= 20
        results[:mrr] += 1/rank.to_f

        puts "#{query} -- #{results} MRR: #{results[:mrr] / results[:count].to_f * 100}" if results[:count]%25==0
      end
      puts "--------- #{results} MRR: #{results[:mrr] / results[:count].to_f * 100} ----------"
    end

    def search_desc(image, *params)
    end

    def reversed_benchmark(*params)

      results = {count: 0, top1: 0, top5: 0, top10: 0, top15: 0, top20: 0, mrr: 0}
      test_images.each do |image_id, image|

        rank_list = search_desc(image, *params)
        results[:count] += 1

        ranks = rank_list.each_index.select { |index| image.descriptions.include?(rank_list[index]) }
        ranks.map! { |index| index+1 }
        results[:top1] += ranks.count { |rank| rank == 1 }
        results[:top5] += ranks.count { |rank| rank <= 5 }
        results[:top10] +=ranks.count { |rank| rank <= 10 }
        results[:top15] +=ranks.count { |rank| rank <= 15 }
        results[:top20] +=ranks.count { |rank| rank <= 20 }
        results[:mrr] += ranks.sort.each_with_index.inject(0) { |sum, (rank, i )| sum + (i+1)/rank.to_f }

        puts "#{image_id} -- #{results} MRR: #{results[:mrr] / results[:count].to_f * 100}" if results[:count]%25==0
      end
      puts "--------- #{results} MRR: #{results[:mrr] / results[:count].to_f * 100} ----------"

    end


    def load_test_train_data
      index = 0
      guessing_index = 0
      test_images = []

      File.open(File.expand_path("public/datasets/Flickr_8k.testImages.txt", Rails.root)).each_line { |image| test_images << image.strip }
      File.open(File.expand_path("public/datasets/Flickr_8k.trainImages.txt", Rails.root)).each_line { |image| @train << image.strip }

      File.open(File.expand_path("public/datasets/Flickr8k.lemma.token.txt", Rails.root)).each_line do |line|
        description = line.split("\t")
        image_id = doc_id_to_image_id(description[0])

        # guessing_index = Random.rand(5) if index%5 ==0
        guessing_index = index/5%5 if index%5 ==0
        @test[image_id] = description[1].strip if test_images.include?(image_id) && index%5 == guessing_index
        index += 1
      end
    end

  end
end
