module MachineLearning
  class EmbeddingsBasedSearch < ImageSearchBase

    def initialize
      @images = ImageSearchBase.instance.images
      @train = ImageSearchBase.instance.train
      @test = ImageSearchBase.instance.test
      @u = ImageSearchBase.instance.u
      @v = ImageSearchBase.instance.v
    end

    def search_best_match(query)
      train_images.sort_by { |_image_id, image| image.document_vectors_score(query, :aggregate_ranking_best) }.last[1]
    end

    def search(query)
      best_image = search_best_match(query)

      test_images.sort_by do |_image_id, image|
        image.image_similarity(best_image)
      end.reverse
    end

  end
end
