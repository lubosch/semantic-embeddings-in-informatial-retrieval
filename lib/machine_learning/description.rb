module MachineLearning
  class Description

    LAMBDA = 0.7
    @@mixed_lambda = 0.7

    attr_accessor :description, :probabilities, :vector, :multimodal_vector
    cattr_accessor :mixed_lambda

    def initialize(text, documents_model=nil, u=nil)
      @description = text
      tokens = preprocess_sentence(text)
      @probabilities = words_probabilities(tokens)
      @vector = Analogies.instance.sentence_vector(tokens)
      tokens.each { |word| documents_model[word] ? documents_model[word] += 1 : documents_model[word] = 1 } if documents_model
      @multimodal_vector = u*@vector if u && @vector && @vector.size == u.size/300
    end

    # sim = Math.log2((1-LAMBDA) * query_word_global_probability + LAMBDA * @probabilities[query_word].to_f)
    # normalized
    def document_unigram_score(query)
      query = preprocess_sentence(query)
      query.inject(0) do |result, query_word|
        query_word_global_probability = ImageSearcher.instance.documents_model[query_word]
        if query_word_global_probability

          # query word is in description?
          sim = @probabilities[query_word] ?
              Math.log2(2 -LAMBDA) + Math.log2(@probabilities[query_word] + 1) - query_word_global_probability :
              Math.log2(query_word_global_probability + 1)

          result + sim
        else
          # ignore words with no information in descriptions
          result
        end
      end./ query.size
    end

    def document_mixed_score(query)
      @@mixed_lambda * document_unigram_score(query) + (1-@@mixed_lambda) * (document_vectors_score(query)+1)/2
    end

    def document_vectors_score(query)
      query = Description.new(query)
      @vector && query ? Base::cosine_similarity(query.vector, @vector) : 0
    end

    def words_probabilities(tokens)
      probabilities = {}
      tokens.each { |word| probabilities[word] ? probabilities[word] += 1 : probabilities[word] = 1 }
      probabilities.each { |k, v| probabilities[k] = v / tokens.size.to_f }
      probabilities
    end

    def preprocess_sentence(sentence)
      sentence.gsub(/[\W\d]+/, ' ').split.map(&:downcase) - Base::STOPWORDS
    end

    def image_cca_similarity(image, k)
      new_size = 3 * k
      Base.cosine_similarity(@multimodal_vector[0...new_size], image.multimodal_vector[0...new_size])
    end

  end
end

