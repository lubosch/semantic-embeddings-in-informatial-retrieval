module MachineLearning
  class ImageSearcher
    include Singleton

    attr_accessor :documents_model, :images

    def initialize
      @documents_model = {}
      @images = {}
    end

    def clear_and_pre_train!(train=nil)
      @documents_model.clear
      @images.clear
      pre_train!(train)
    end

    def pre_train!(train_data=nil)
      return nil if @documents_model.present?
      train_data ||= File.open(File.expand_path("public/datasets/Flickr8k.lemma.token.txt", Rails.root))
      train_data.each do |line|
        document = line.split("\t")

        image_id = doc_id_to_image_id(document[0])
        image = @images[image_id] ||= Image.new(image_id)
        image.add_description(document[1], @documents_model)
      end

      sum = @documents_model.values.sum.to_f
      @documents_model.each { |word, occurrences| @documents_model[word] = occurrences / sum }
    end

    def doc_id_to_image_id(doc_id)
      doc_id.gsub(/#\d+/, '')
    end

    def search(query, method, aggregation_method)
      @images.sort_by do |_image_id, image|
        case method
          when 'unigram'
            image.document_unigram_score(query, aggregation_method)
          when 'vectors'
            image.document_vectors_score(query, aggregation_method)
          when 'mixed'
            image.document_mixed_score(query, aggregation_method)
        end
      end.reverse
    end

   def benchmark(method, aggregation_method, just_test)
      raise NoDatasetLoaded if method != 'unigram' && Analogies.instance.matrix.blank?
      test = load_test_train_data(just_test)

      results = {count: 0, top1: 0, top5: 0, top10: 0, top15: 0, top20: 0, mrr: 0}
      test.each do |id, query|
        rank_list = search(query, method, aggregation_method)
        results[:count] += 1

        rank = rank_list.find_index { |r| r[0] == id } + 1

        results[:top1] += 1 if rank == 1
        results[:top5] += 1 if rank <= 5
        results[:top10] += 1 if rank <= 10
        results[:top15] += 1 if rank <= 15
        results[:top20] += 1 if rank <= 20
        results[:mrr] += 1/rank.to_f

        puts "#{rank_list[0][0]} #{id} -  #{rank_list[0][0] == id} #{query} -- #{results}"
      end
    end

    def load_test_train_data(just_test)
      index = 0
      guessing_index = 0
      test = {}
      train = []
      test_images = []

      File.open(File.expand_path("public/datasets/Flickr_8k.testImages.txt", Rails.root)).each_line { |image| test_images << image.strip }

      File.open(File.expand_path("public/datasets/Flickr8k.lemma.token.txt", Rails.root)).each_line do |line|
        description = line.split("\t")
        image_id = doc_id_to_image_id(description[0])
        next if just_test && test_images.exclude?(image_id)

        guessing_index = Random.rand(5) if index%5 ==0

        (index%5 == guessing_index && test_images.include?(image_id)) ? test[image_id] = description[1].strip : train << line
        index+=1
      end
      clear_and_pre_train!(train)
      test
    end

  end
end
