module MachineLearning
  class Analogies
    include Singleton

    attr_accessor :matrix

    def initialize
      @matrix = nil
      @source = nil
    end

    def analyze_matrix(file)
      @matrix = load_matrix_from_file(file) if @source != file.original_filename
      @source = file.original_filename
    end

    def load_matrix_from_file(file)
      file_path = file.path
      matrix = {}
      File.open(file_path).each_line do |line|
        line = line.split
        word = line.shift
        vector = NVector.to_na(line.map(&:to_f))
        normalized = vector.normalize
        matrix[word] = {vector: vector, normalized: normalized}
      end
      matrix
    end

    def guess(word1, word2, query, method)
      return 'Dataset not loaded' if @matrix.nil?
      v1, v2, q = find_input_vectors(word1, word2, query)
      return 'Identical vectors' if v1 == v2
      return 'Not found in dataset' if v1.nil? || v2.nil? || q.nil?

      case method
        when 'addition'
          guess_by_addition(v1, v2, q)
        when 'direction'
          guess_by_direction(v1, v2, q)
        when 'multiplication'
          guess_by_multiplication(v1, v2, q)
      end
    end

    def find_input_vectors(word1, word2, query)
      v1 = @matrix[word1.downcase]
      v2 = @matrix[word2.downcase]
      q = @matrix[query.downcase]
      return v1, v2, q
    end

    def guess_by_addition(v1, v2, q)
      ulti_vector = (q[:vector] - v1[:vector] + v2[:vector]).normalize

      highest_scored = {:value => 0, :word => ''}
      @matrix.each do |word, d|
        similarity = Base::normalized_cosine_similarity(d[:normalized], ulti_vector)
        highest_scored[:value], highest_scored[:word] = similarity, word if highest_scored[:value] < similarity
      end
      puts highest_scored[:value]
      highest_scored[:word]
    end

    def guess_by_direction(v1, v2, q)
      ulti_vector = (v1[:vector] - v2[:vector])
      ulti_vector_magnitude = (v1[:vector] - v2[:vector]).magnitude
      highest_scored = {:value => 0, :word => ''}
      @matrix.each do |word, d|
        query_vector = q[:vector] - d[:vector]
        similarity = Base::cosine_similarity(ulti_vector, query_vector, ulti_vector_magnitude)
        highest_scored[:value], highest_scored[:word] = similarity, word if highest_scored[:value] < similarity
      end
      puts highest_scored[:value]
      highest_scored[:word]
    end

    def guess_by_multiplication(v1, v2, q)
      highest_scored = {:value => 0, :word => ''}
      @matrix.each do |word, d|
        similarity = Base::multiplication_similarity(v1[:normalized], v2[:normalized], q[:normalized], d[:normalized])
        highest_scored[:value], highest_scored[:word] = similarity, word if highest_scored[:value] < similarity
      end
      puts highest_scored[:value]
      highest_scored[:word]
    end

    def benchmark(method)
      return 'Dataset not loaded' if @matrix.nil?
      groups = []
      File.open(File.expand_path("public/datasets/questions-words.txt", Rails.root)).each_line do |line|
        if line[0] == ':'
          groups << {:name => [line[2..-1]], :success => 0, :count => 0}
          next
        end
        last_group = groups.last
        # random line from group selector, max 5 from each group
        next if Random.rand < 0.95 || last_group[:count] > 4

        words = line.split.map { |value| value.strip.downcase }
        last_group[:count] += 1
        guess = guess(*words[0..2], method)
        last_group[:success] += 1 if guess== words[3]
        puts "#{words} - #{guess}"
        puts groups if last_group[:count] == 5
      end
    end

    def normalized_sentence_vector(sentence)
      vector = sentence_vector(sentence)
      vector ? vector.normalize : vector
    end

    def sentence_vector(sentence)
      return nil if @matrix.blank?
      vectors = sentence.map { |vector| @matrix[vector][:vector] if @matrix[vector] }.compact
      vectors.inject(&:+)
    end


  end
end
