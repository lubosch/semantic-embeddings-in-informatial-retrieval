module MachineLearning
  class Image

    attr_accessor :descriptions, :id, :vector, :multimodal_vector

    def initialize(id, vector=nil, v=nil)
      @id = id
      @descriptions = []
      @vector = vector
      # binding.pry
      @multimodal_vector = v*vector if v && vector && v.size/300 == vector.size
    end

    def add_description(description, document_model=nil, v=nil)
      @descriptions << Description.new(description, document_model, v)
    end

    def aggregate_ranking_avg(scores)
      scores.reduce(:+).to_f / scores.size
    end

    def aggregate_ranking_best(scores)
      scores.sort.last
    end

    def document_unigram_score(query, aggregation_method)
      scores = []
      @descriptions.each { |description| scores << description.document_unigram_score(query) }
      send(aggregation_method, scores)
    end

    def document_vectors_score(query, aggregation_method)
      scores = []
      @descriptions.each { |description| scores << description.document_vectors_score(query) }
      send(aggregation_method, scores)
    end

    def document_mixed_score(query, aggregation_method)
      scores = []
      @descriptions.each { |description| scores << description.document_mixed_score(query) }
      send(aggregation_method, scores)
    end

    def image_similarity(image)
      Base.cosine_similarity(@vector, image.vector)
    end

    def image_cca_similarity(query, k)
      new_size = 3 * k
      Base.cosine_similarity(@multimodal_vector[0...new_size], query.multimodal_vector[0...new_size])
    end

  end
end
