module MachineLearning
  class Base

    STOPWORDS = ["i", "a", "about", "an", "are", "as", "at", "be", "by", "com", "for", "from", "how", "in", "is", "it", "of", "on", "or", "that", "the", "this", "to", "was", "what", "when", "where", "who", "will", "with", "the", "www"]

    def self.cosine_similarity(a_vector, b_vector, a_vector_magnitude=nil, b_vector_magnitude=nil)
      (a_vector * b_vector) / ((a_vector_magnitude || a_vector.magnitude) * (b_vector_magnitude || b_vector.magnitude))
    end

    def self.normalized_cosine_similarity(a_vector, b_vector)
      a_vector * b_vector
    end

    def self.multiplication_similarity(v1, v2, q, d)
      ((normalized_cosine_similarity(d, q) + 1) / 2) * ((normalized_cosine_similarity(d, v2) + 1) / 2) / (((normalized_cosine_similarity(d, v1) + 1) / 2) + 0.001)
    end

    def self.magnitude(vector)
      Math::sqrt(vector * vector)
    end

  end
end

class NVector

  def magnitude
    Math::sqrt(self * self)
  end

  def normalize
    magnitude = self.magnitude
    self / magnitude unless magnitude.zero?
  end

end
