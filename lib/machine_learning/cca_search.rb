module MachineLearning
  class CcaSearch < ImageSearchBase

    def initialize
      @images = ImageSearchBase.instance.images
      @train = ImageSearchBase.instance.train
      @test = ImageSearchBase.instance.test
      @u = ImageSearchBase.instance.u
      @v = ImageSearchBase.instance.v
    end

    def search(query, k=100)
      query = Description.new(query, nil, @u)
      test_images.sort_by do |_image_id, image|
        image.image_cca_similarity(query, k)
      end.reverse
    end

    def search_desc_img(image, k)
      search_desc(@images[image], k)
    end

    def search_desc(image, k)
      test_descriptions.sort_by do |descr|
        descr.image_cca_similarity(image, k)
      end.reverse
    end

    def benchmark(k)
      if k
        puts "super"
        super(k)
      else
        (10..100).step(10) do |k|
          puts "------- ---------------- ||||||||||||||     #{k}        |||||||||| ----------- ----------"
          super(k)
        end
      end
    end

    def reversed_benchmark(k)
      if k
        puts "super"
        super(k)
      else
        (10..100).step(10) do |k|
          puts "------- ---------------- ||||||||||||||     #{k}        |||||||||| ----------- ----------"
          super(k)
        end
      end
    end

  end
end
