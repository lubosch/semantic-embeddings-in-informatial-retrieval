class ImageSearcherController < ApplicationController

  before_action :validate_dataset, :only => [:benchmark, :search]

  def index

  end

  def search
    query = params[:q]
    method = params[:image_search_method]
    aggr_method = params[:aggregation_method].to_sym
    MachineLearning::Description.mixed_lambda = params[:mixed_lambda].to_f
    MachineLearning::ImageSearcher.instance.pre_train!
    @ranked_docs = MachineLearning::ImageSearcher.instance.search(query, method, aggr_method)
  end

  def benchmark
    method = params[:image_search_method]
    just_test = params[:just_test] == '1'
    aggr_method = params[:aggregation_method].to_sym
    MachineLearning::Description.mixed_lambda = params[:mixed_lambda].to_f

    @ranked_docs = MachineLearning::ImageSearcher.instance.benchmark(method, aggr_method, just_test)
  end

  def validate_dataset
    raise MachineLearning::NoDatasetError if params[:image_search_method] != 'unigram' && MachineLearning::Analogies.instance.matrix.blank?
  end

end
