class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from MachineLearning::NoDatasetError do
    respond_to do |format|
      format.js { render 'machine_learning/no_dataset_error' }
    end
  end

end
