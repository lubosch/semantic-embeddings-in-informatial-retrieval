class AnalogiesController < ApplicationController

  def index

  end

  def create
    MachineLearning::Analogies.instance.analyze_matrix(params['glove-file'])
    # MachineLearning::ImageSearcher.instance.clear_and_pre_train!
    MachineLearning::ImageSearchBase.instance.pre_train!
    flash[:success] = 'Matrix loaded'
  end

  def guess
    @answer = MachineLearning::Analogies.instance.guess(params[:v1].strip, params[:v2].strip, params[:question].strip, params[:analogy_method])
  end

  def benchmark
    MachineLearning::Analogies.instance.benchmark(params[:analogy_method])
  end

end
