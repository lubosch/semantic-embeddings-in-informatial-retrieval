class CcaSearchController < ApplicationController

  before_action :validate_dataset, :only => [:benchmark, :search]

  def index

  end

  def search
    query = params[:q]
    k = params[:k].to_i if params[:k].present?

    @ranked_docs = MachineLearning::CcaSearch.instance.search(query, k)
  end

  def search_description
    @query = params[:q]
    k = params[:k].to_i if params[:k].present?

    @ranked_desc = MachineLearning::CcaSearch.instance.search_desc_img(@query, k)
  end

  def benchmark

    k = params[:k].to_i if params[:k].present?
    if params.keys.include?('image_search_method')
      @ranked_docs = MachineLearning::CcaSearch.instance.benchmark(k)
    else
      @ranked_docs = MachineLearning::CcaSearch.instance.reversed_benchmark(k)
    end
  end

  def validate_dataset
    raise MachineLearning::NoDatasetError if params[:image_search_method] != 'unigram' && MachineLearning::Analogies.instance.matrix.blank?
  end

end
