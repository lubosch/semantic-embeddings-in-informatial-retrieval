class EmbeddingsBasedSearchController < ApplicationController

  before_action :validate_dataset, :only => [:benchmark, :search]

  def index

  end

  def search
    query = params[:q]
    @ranked_docs = MachineLearning::EmbeddingsBasedSearch.instance.search(query)
  end

  def benchmark
    @ranked_docs = MachineLearning::EmbeddingsBasedSearch.instance.benchmark
  end

  def validate_dataset
    raise MachineLearning::NoDatasetError if params[:image_search_method] != 'unigram' && MachineLearning::Analogies.instance.matrix.blank?
  end

end
